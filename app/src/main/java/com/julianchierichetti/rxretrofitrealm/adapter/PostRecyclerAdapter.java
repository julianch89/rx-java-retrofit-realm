package com.julianchierichetti.rxretrofitrealm.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.julianchierichetti.rxretrofitrealm.R;
import com.julianchierichetti.rxretrofitrealm.model.Post;

import java.util.ArrayList;
import java.util.List;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

/**
 * Created by julian on 20-Feb-17.
 */

public class PostRecyclerAdapter extends RealmRecyclerViewAdapter<Post,PostRecyclerAdapter.ViewHolder>{


    public PostRecyclerAdapter(@NonNull Context context, @Nullable OrderedRealmCollection<Post> data, boolean autoUpdate) {
        super(context, data, autoUpdate);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post,parent,false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mTitleView.setText(getData().get(position).title);
        holder.mBodyView.setText(getData().get(position).body);
    }

    @Override
    public int getItemCount() {
        return getData().size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder{

        private TextView mTitleView;
        private TextView mBodyView;

        public ViewHolder(View itemView) {
            super(itemView);
            mTitleView = (TextView)  itemView.findViewById(R.id.item_post_title);
            mBodyView = (TextView) itemView.findViewById(R.id.item_post_body);
        }
    }
}
