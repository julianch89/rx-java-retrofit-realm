package com.julianchierichetti.rxretrofitrealm.backend;

import com.julianchierichetti.rxretrofitrealm.model.Post;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by julian on 20-Feb-17.
 */

public interface PostService {
    @GET("posts")
    Observable<List<Post>> getPosts();


}
