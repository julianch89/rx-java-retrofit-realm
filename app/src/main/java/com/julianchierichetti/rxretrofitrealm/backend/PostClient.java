package com.julianchierichetti.rxretrofitrealm.backend;

import android.support.annotation.NonNull;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.julianchierichetti.rxretrofitrealm.model.Post;

import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

/**
 * Created by julian on 20-Feb-17.
 */

public class PostClient {

    private static final String BACKEND_BASE_URL = "https://jsonplaceholder.typicode.com/";

    private static PostClient instance;
    private PostService postService;

    private PostClient() {
        final Gson gson =
                new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
        final Retrofit retrofit = new Retrofit.Builder().baseUrl(BACKEND_BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        postService = retrofit.create(PostService.class);
    }

    public static PostClient getInstance() {
        if (instance == null) {
            instance = new PostClient();
        }
        return instance;
    }

    public Observable<List<Post>> getPosts() {
        return postService.getPosts();
    }
}
