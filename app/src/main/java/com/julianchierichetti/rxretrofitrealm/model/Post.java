package com.julianchierichetti.rxretrofitrealm.model;

import io.realm.RealmObject;

/**
 * Created by julian on 20-Feb-17.
 */

public class Post extends RealmObject {
    public long userId;
    public long id;
    public String title;
    public String body;

    public Post(){
    }
}
