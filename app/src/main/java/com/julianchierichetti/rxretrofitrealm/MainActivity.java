package com.julianchierichetti.rxretrofitrealm;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.julianchierichetti.rxretrofitrealm.adapter.PostRecyclerAdapter;
import com.julianchierichetti.rxretrofitrealm.backend.PostClient;
import com.julianchierichetti.rxretrofitrealm.model.Post;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private PostRecyclerAdapter mAdapter;
    private List<Post> mDataSet = new ArrayList<>();
    private Button mRefreshButton;
    private Subscription mSubscription;
    private Realm mRealm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRealm = Realm.getDefaultInstance();
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter
        mAdapter = new PostRecyclerAdapter(this,mRealm.where(Post.class).findAllAsync(),true);
        mRecyclerView.setAdapter(mAdapter);
        mRefreshButton = (Button) findViewById(R.id.button);
        mRefreshButton.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRecyclerView.setAdapter(null);
        if(mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        mRealm.close();

    }

    @Override
    public void onClick(View view) {
        mSubscription = PostClient.getInstance().getPosts()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.newThread())
                .subscribe(new Observer<List<Post>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<Post> posts) {
                        Realm realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        for(Post post:posts) {
                           realm.copyToRealm(post);
                        }
                        realm.commitTransaction();
                    }
                });
    }
}
